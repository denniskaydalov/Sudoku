import java.util.Scanner;
import java.util.Random;

/**
 * This class defines the behaviour of the Sudoku class, which defines the main behaviour of the sudoku input and output, including opponent and win conditions.
 * 
 * @author Dennis Kaydalov
 * 
 * @version December 13, 2022
 */
public class Sudoku {
    /**
     * Main method to test the Sudoku class
     * 
     * @param args - Command line arguments
     */
    public static void main(String[] args) {
        Board player = new Board();
        Board opponent = new Board();
        //boolean for the main game loop
        boolean gameRunning = true;

        //randomly assign values to the player and opponent board
        player.randomlyPopulateBoard();
        opponent.randomlyPopulateBoard();

        Scanner scanner = new Scanner(System.in);

        //get input for difficult/random types
        introductionInput(player, opponent, scanner);

        while(gameRunning) {
            System.out.println();
            player.displayBoard(false);
            System.out.println();

            //have the players move
            userMove(player, scanner);
            computerMove(opponent, scanner);

            //win/lose conditions
            if(player.getNumbersLeft() == 0) {
                System.out.println("YOU WON! CONGRATULATIONS FOR COMPLETING THE PUZZLE");
                gameRunning = false;
            }
            if(opponent.getNumbersLeft() == 0) {
                System.out.println("You lost the the opponent :(");
                gameRunning = false;
            }
        }
    }
    
     /**
     * The computerMove method defines the behaviour behind the compuer moving, it accepts the compuer board and a scanner and calculates based on difficulty the best move for the computer to do.
     * 
     * @param board board variable which is a reference to the compuer board
     * @param scanner scanner to use in the method
     */
    public static void computerMove(Board board, Scanner scanner) {
        Random random = new Random();
        int row;
        int column;
        int guess;
        //bool for looping through not possible row/column picks
        boolean notChosen = true;
        //bool for checking if the computer guessed correctly
        boolean correct = true;
        //make decisions based on the board's difficulty
        switch(board.getDifficulty()) {
            case EASY:
                row = random.nextInt(9) + 1;
                column = random.nextInt(9) + 1;
                guess = random.nextInt(9) + 1;
                //break if the square is not available 
                if(!board.checkAvailability(row, column)) break;
                correct = board.setNumber(row, column, guess);
                break;
            case NORMAL:
                //loop through until a valid row and column have been picked
                while(notChosen) {
                    row = random.nextInt(9) + 1;
                    column = random.nextInt(9) + 1;
                    guess = random.nextInt(9) + 1;
                    //only guess if the square is available
                    if(board.checkAvailability(row, column)) {
                        if(!board.setNumber(row, column, guess)) correct = false;
                        notChosen = false;
                    }
                }
                break;
            case HARD:
                //loop through until a valid row and column have been picked, and get the right answer
                while(notChosen) {
                    row = random.nextInt(9) + 1;
                    column = random.nextInt(9) + 1;
                    //only guess if the square is available
                    if(board.checkAvailability(row, column)) {
                        if(!board.setNumber(row, column, board.getTrueValue(row, column))) correct = false;
                        notChosen = false;
                    }
                }
                break;
        }
        if(correct) System.out.println("The computer guess correctly! It's only got " + board.getNumbersLeft() + " numbers left!");
        else System.out.println("The computer guessed incorrectly");
    }

     /**
     * The userMove method defines the behaviour behind having the user pick a row, column and guess number, includes bounds/input mismatch checking and bad choices.
     * 
     * @param board holds a reference to the player board
     * @param scanner holds the scanner for this method to use
     */
    public static void userMove(Board board, Scanner scanner) {
        //get a row choice
        int row = inputInt("Enter a row number within the range of 1 to 9 (inclusive): ", 1, 9, scanner);
        //get a column choice
        int column = inputInt("Enter a column number within the range of 1 to 9 (inclusive): ", 1, 9, scanner);

        //check if the row or column are possible
        if(!board.checkAvailability(row, column)) {
            System.out.println("That square is not an available to guess, as it's non-empty! And contains the value: " + board.getValue(row, column));
            //if the move is invalid, simply run the method again for new inputs
            userMove(board, scanner);
            return;
        }

        //get a guess number
        int guess = inputInt("Enter a guess number within the range of 1 to 9 (inclusive): ", 1, 9, scanner);
        System.out.println();

        //check if the guess was successful
        if(board.setNumber(row, column, guess))
            System.out.println("Correct! Only " + board.getNumbersLeft() + " numbers left to guess!");
        else System.out.println("You were wrong :(");
    }

     /**
     * The introductionInput method defines the behaviour for the user entering the difficulty and randomness of the board. It also hides numbers from the boards based on difficulty and random type.
     * 
     * @param board player board 
     * @param opponent opponent board
     * @param scanner scanner for input
     */
    public static void introductionInput(Board board, Board opponent, Scanner scanner) {
        Random random = new Random();
        //get a random type
        String inputRandomType = inputString("Please enter a randomness type to determine the style of your sudoku board, which is either: random, only_border or keep_border: ", new String[] { "RANDOM", "ONLY_BORDER", "KEEP_BORDER"}, scanner);
        RandomType randomType = RandomType.RANDOM;
        //assign the randomtype variable an actual elent from the enum as it is right now in string format from the user input
        switch(inputRandomType) {
            case "RANDOM":
                randomType = RandomType.RANDOM;
                break;
            case "ONLY_BORDER":
                randomType = RandomType.ONLY_BORDER;
                break;
            case "KEEP_BORDER":
                randomType = RandomType.KEEP_BORDER;
                break;
        }
        //get a difficulty type
        String inputDifficultyType= inputString("Please enter a difficulty type to determine the dififculty of your sudoku board, and your opponent, which is either: easy, normal or hard: ", new String[] { "EASY", "NORMAL", "HARD"}, scanner);
        //assign the difficultyType variable an actual elent from the enum as it is right now in string format from the user input
        DifficultyType difficultyType = DifficultyType.EASY;
        switch(inputDifficultyType) {
            case "EASY":
                difficultyType = DifficultyType.EASY;
                break;
            case "NORMAL":
                difficultyType = DifficultyType.NORMAL;
                break;
            case "HARD":
                difficultyType = DifficultyType.HARD;
                break;
        }
        //set difficulties for the player and opponent
        board.setDifficulty(difficultyType);
        opponent.setDifficulty(difficultyType);
        //set the seed as a random value so that both board's hidden values are the same, to keep the game fair
        int seed = random.nextInt(Integer.MAX_VALUE);
        //randomly hide elements from the player and opponent boards
        board.randomlyHideBoard(randomType, difficultyType, seed);
        opponent.randomlyHideBoard(randomType, difficultyType, seed);
    }

    /**
     * The inputInt method defines input validation for integers with min and max
     * 
     * @param min min value for the integer that the user inputs should have
     * @param max max value for the integer that the user inputs should have
     * @param message message to print when requesting input
     * @param scanner scanner for the method to use
     * @return int - return an integer which fit the defined bounds from the user
     */
    public static int inputInt(String message, int min, int max, Scanner scanner) {
        //inifite loop until return to make sure proper input has been entered from the user
        while(true) {
            System.out.print(message);
            String inputString = scanner.nextLine();
            try {
                int input = Integer.parseInt(inputString);
                //check bounds
                if(input < min || input > max) {
                    System.out.println(input + " is out of range!");
                    //retry
                    continue;
                }
                return input;
            }
            catch(Exception e) {
                System.out.println("Please enter a valid integer!");
            }
        }
    }

    /**
     * The inputString method defines input validation for strings for cases where the string should be one of multiple choices 
     * 
     * @param options options array for what the user input can be (can be one of the options), additional constraint is that the array should only contain options in all capitals
     * @param message message to print when requesting input
     * @param scanner scanner for the method to use
     * @return String - returns the approriate string which fit the requirements
     */
    public static String inputString(String message, String[] options, Scanner scanner) {
        while(true) {
            System.out.print(message);
            String inputString = scanner.nextLine();
            //linear search to make sure that the user inputed string is valid in options array
            for(String option : options) {
                //format the user input, and check against one of the options
                if(inputString.toUpperCase().trim().equals(option)) return option;
            }
            System.out.println(inputString + " was not one of the options!");
        }
    }
}
