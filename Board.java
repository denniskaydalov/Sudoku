import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * The DifficultyType enum defines values for different difficulty types for the game
 * 
 * @author Dennis Kaydalov
 * 
 * @version December 13, 2022
 */
enum DifficultyType {
    EASY,
    NORMAL,
    HARD;
}

/**
 * The RandomType enum defines values for different randomness types for the boards during the game, RANDOM being truly random, KEEP_BORDER keeping the border intact, and ONLY_BORDER only removing from border
 * 
 * @author Dennis Kaydalov
 * 
 * @version December 13, 2022
 */
enum RandomType {
    RANDOM,
    KEEP_BORDER,
    ONLY_BORDER;
}

/**
 * The Board class defines the behaviour of the board, including randomness, number generation and helper methods for validation and value setting
 * 
 * @author Dennis Kaydalov
 * 
 * @version December 13, 2022
 */

public class Board {
    //board which holds all values of the solved Sudoku
    private int[][] board = new int[9][9];
    //board which holds the values displayed to the player
    private int[][] display_board = new int[9][9];
    //difficulty of the board
    private DifficultyType difficulty;

    /**
     * The Board constructor clears all boards
     */
    public Board() {
        clearBoard(board);
        clearBoard(display_board);
    }

    /**
     * The clearBoard method defines the behaviour of setting all values of the board to 0
     *
     * @param board board variable to clear
     */
    public void clearBoard(int[][] board) {
        for(int i = 0; i < board.length; ++i) {
            for(int j = 0; j < board.length; ++j) {
                board[i][j] = 0;
            }
        }
    }

    /**
     * The getDifficulty accessor is used for accessing the difficultyType
     *
     * @return DifficultyType - DifficultyType of the object
     */
    public DifficultyType getDifficulty() {
        return difficulty;
    }

    /**
     * The getDifficulty setter is used for setting the value of the object's difficulty
     *
     * @param difficulty difficult to set in the object
     */
    public void setDifficulty(DifficultyType difficulty) {
        this.difficulty = difficulty;
    }

    /**
     * The displayBoard method defines the behaviour for neatly displaying the entire board, with a boolean parameter for the case where one wants to display the board with all answers displayed
     *
     * @param cheat boolean for when one wants to display answers for the sudoku
     */
    public void displayBoard(boolean cheat) {
        //indent
        System.out.print("    ");
        //print out all row nums
        int rowNum = 1;
        for(int i = 1; i <= 9; ++i) {
            // indent approriately
            if(i == 4 || i == 7)
                System.out.print("      " + i + "   ");
            else System.out.print(i + "   ");
        }
        //ident down
        System.out.println();
        System.out.println();
        //loop through all rows
        for(int i = 0; i < 9; ++i) {
            //print the row number with an indent of 2 spaces
            System.out.print(rowNum + "  ");
            rowNum++;
            //loop through all columns
            for(int j = 0; j < 9; ++j) {
                //indent
                System.out.print(" ");
                //print out actual value if cheat true
                if(cheat == true) System.out.print(board[i][j]);
                //print out display value otherwise
                else if(display_board[i][j] == 0) System.out.print('~');
                else System.out.print(display_board[i][j]);
                //indent
                System.out.print(" ");
                //print out vertical bars for separation
                if(j < 8) {
                    if((j + 1) % 3 == 0) System.out.print("   |   ");
                    else System.out.print("|");
                }
            }
            //newline for formatting
            System.out.println();
            //check if it's not the last column
            if(i < 8) {
                //formatting indent
                System.out.print("   ");
                //print approriate bars for seperation
                if((i + 1) % 3 == 0) {
                    //loop through the length of all the sub-boards(3x3) and indents
                    for(int j = 0; j < 50; ++j) {
                        //bars between sub-boards
                        if(j == 14 || j == 32) System.out.print("|");
                        else System.out.print(" ");
                    }
                    //newline for newline formatting
                    System.out.println();
                }
                //loop through all columns
                for(int j = 0; j < 9; ++j) {
                    //print indent at start of the line
                    if(j == 0 && (i + 1) % 3 == 0) System.out.print("   ");
                    //print seperation line when outside a sub-board(3x3)
                    if((j+1) % 3 == 0 && j > 0) {
                        System.out.print("---");
                        if((i + 1) % 3 == 0 && j < 8) System.out.print("---+---");
                        else if(j < 8) System.out.print("   |   ");
                    }
                    //smaller seperate
                    else {
                        System.out.print("---");
                        if(j < 8 && (i + 1) % 3 != 0) System.out.print("+");
                        else System.out.print("-");
                    }
                }
                //seperate sub-boards(3x3)
                if((i + 1) % 3 == 0) {
                    System.out.println();
                    System.out.print("   ");
                    for(int j = 0; j < 50; ++j) {
                        if(j == 14 || j == 32) System.out.print("|");
                        else System.out.print(" ");
                    }
                }
                //newline for formatting
                System.out.println();
            }
        }
    }

    /**
     * The randomlyPopulateBoard method defines the behaviour of randomy assigning integers for a solve board for the board variable in the class object
     */
    public void randomlyPopulateBoard() {
        //store integers for inserting
        ArrayList<Integer> numbers = new ArrayList<>();
        for(int i = 1; i <= 9; ++i) numbers.add(i);
        //randomly shuffle the integers
        Collections.shuffle(numbers);
        //turn variable for the algorithm for randomly generating the integers
        for(int turn = 0; turn < 9; ++turn) {
            //assign the integers to the board
            for(int i = 0; i < 9; ++i) {
                board[turn][i] = numbers.get(i);
                display_board[turn][i] = numbers.get(i);
            }
            //shift depending on which turn it is to shift
            if(turn == 2 || turn == 5)
                Collections.rotate(numbers, -1);
            else Collections.rotate(numbers, -3);
        }
    }

    /**
     * desc
     * 
     * @param type randomtype to define how to erase board
     * @param difficulty difficultyType to define how many integers to erase
     * @param seed seed to maintain same erase across boards in a game (player and opponent)
     */
    public void randomlyHideBoard(RandomType type, DifficultyType difficulty, int seed) {
        Random random = new Random(seed);
        int removed = 0;
        //randomly generate the amount of integers to erase based on difficulty
        switch(difficulty) {
            case EASY:
                removed = random.nextInt(20) + 40;
                break;
            case NORMAL:
                removed = random.nextInt(15) + 25;
                break;
            case HARD:
                removed = random.nextInt(8) + 17;
                break;
        }
        int row; 
        int column; 
        //choose a integer to erase based on randomType, and erase it
        while(removed < 81) {
            row = random.nextInt(9);
            column = random.nextInt(9);
            //constaints
            if(type == RandomType.RANDOM)
                if(display_board[row][column] == 0) continue;
            if(type == RandomType.KEEP_BORDER)
                if(row % 8 == 0 || column % 8 == 0) continue;
            if(type == RandomType.ONLY_BORDER)
                if(row % 8 != 0 && column % 8 != 0) continue;
            display_board[row][column] = 0;
            removed++;
        }
    }

    /**
     * The getNumbersLeft method defines the behaviour of getting the amount of numbers the caller has left to guess
     * 
     * @return int - the value of how many numbers the caller has left
     */
    public int getNumbersLeft() {
        int differentNumbers = 0;
        for(int i = 0; i < 9; ++i) {
            for(int j = 0; j < 9; ++j) {
                //increment different numbers if the display number is not the same and the solved number
                if(board[i][j] != display_board[i][j]) differentNumbers++;
            }
        }
        return differentNumbers;
    }

    /**
     * The setNumber method defines the behaviour for setting a value, it accepts a row, column and guess and checks if the guess is right; the method returns a boolean
     * 
     * @param row the row the guess
     * @param column the column the guess
     * @param guess the guess itself
     * @return boolean - weather the guess was successful or not
     */
    public boolean setNumber(int row, int column, int guess) {
        if(checkValidity(row, column, guess)) {
            display_board[row - 1][column - 1] = guess;
            return true;
        }
        else return false;
    }

    /**
     * Checks if the number is available (not 0)
     * 
     * @param row row to check
     * @param column column to check
     * @return boolean - weather the guess was successful or not
     */
    public boolean checkAvailability(int row, int column) {
        return display_board[row - 1][column - 1] == 0;
    }

    /**
     * Get display value from the selected row and column
     * 
     * @param row row to get
     * @param column column to get
     * @return int - the value from the row and column
     */
    public int getValue(int row, int column) {
        return display_board[row - 1][column - 1];
    }

    /**
     * Get true(solved sudoku) value from the selected row and column
     * 
     * @param row row to get
     * @param column column to get
     * @return int - the value from the row and column
     */
    public int getTrueValue(int row, int column) {
        return board[row - 1][column - 1];
    }

    /**
     * The checkValidity helper method defines the behavour of checking if a guess was right
     * 
     * @param row row to guess
     * @param column column to guess
     * @param guess the guess itself
     * @return boolean - weather the guess is valid or not
     */
    private boolean checkValidity(int row, int column, int guess) {
        return board[row - 1][column - 1] == guess;
    }

    /**
     * This is a standard toString() method.  
     * 
     * @return String - The String representation of the current object.
     */
    public String toString() {
        return "The board is of difficultyType: " + difficulty + " and has " + getNumbersLeft() + " numbers left";
    }
}
